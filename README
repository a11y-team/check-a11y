Summary
=======

This allows to check whether the accessibility stack is going right in an X11
environment.

Building
========

Required dependencies:

  pkg-config libdbus-1-dev libatspi2.0-dev

and at least one (or better, all) of:

  libgtk2.0-dev
  libgtk-3-dev
  libqt4-dev
  qtbase5-dev
  qt6-base-dev
  default-jre
  mcs libgtk2.0-cil-dev
  mcs libgtk3.0-cil-dev
  mcs

Run make to build it.

Running
=======

If you are a desktop maintainer, first source env.sh:

  source env.sh

to set variables which are supposed to make the accessibility stack work.

Run make check to perform the checks.  For each enabled toolkit, it will run a
trivial application using it, and test that it can be accessed.  If it can't for
3 times, it will abort.

Troubleshooting
===============

Run ./troubleshoot to debug more precisely potential reasons for the
accessibility stack to be nonfunctional.  This is notably useful for users which
don't have accessibility working for some reason.
