#!/bin/sh

# creates a new, freshly unzipped image


. ./config
. ${HELPERS}

echo $#


rm -rf ${WORKDIR}/${SUITE}
tar xf ${TEMPLATESDIR}/images/${SUITE}.tar -C ${WORKDIR}


if [ $# -gt 0 ]; then
$SYSTEMD --setenv=DEBIAN_FRONTEND=noninteractive apt-get install $1 -y
fi

package=$1


if [ -f overrides/${package}/pre ]; then
mkdir -p ${WORKDIR}/${SUITE}/overrides
cp overrides/${package}/pre ${WORKDIR}/${SUITE}/overrides
fi


if [ -d hooks/${package} ]; then
    mkdir ${WORKDIR}/${SUITE}/hooks
    cp -r hooks/${package} ${WORKDIR}/${SUITE}/hooks
    $SYSTEMD --setenv=DEBIAN_FRONTEND=noninteractive /bin/run-parts -v /hooks/${package} 2>&1
fi
