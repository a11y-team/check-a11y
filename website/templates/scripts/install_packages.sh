#!/bin/sh
# Installs all packages needed for check-a11y

export DEBIAN_FRONTEND=noninteractive

apt-get install make git xinit xvfb libgail-common libatk-adaptor qt-at-spi pkg-config libdbus-1-dev libatspi2.0-dev libgtk2.0-dev libgtk-3-dev libqt5core5a qt5-default libqt4-dev imagemagick  openjdk-8-jdk -y

