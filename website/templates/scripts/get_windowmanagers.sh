#!/bin/sh

# List all packages with "Provides: x-window-manager" in the package description
# This is to be run *inside* the containers, as there might be newer/more packages available

apt-cache show .* |tr "\n" " " | sed 's/Package:/\nPackage:/g'|grep -E "Provides:\s*.*x-window-manager\s*" |grep  -E -o '^Package:\s+[^ ]+\s*' |sort|uniq |awk '{print $2}'
